package de.tudresden.inf.st.sensorsharing;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.wear.widget.BoxInsetLayout;
import android.support.wearable.activity.WearableActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.MessageClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.nio.ByteBuffer;
import java.util.List;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.widget.Toast;

import de.tudresden.inf.st.sensorsharing.common.Utils;

import static de.tudresden.inf.st.sensorsharing.common.Constants.KEY_AMBIENT_PRESSURE;
import static de.tudresden.inf.st.sensorsharing.common.Constants.KEY_BRIGHTNESS;
import static de.tudresden.inf.st.sensorsharing.common.Constants.KEY_COLOR;
import static de.tudresden.inf.st.sensorsharing.common.Constants.KEY_COUNTER;
import static de.tudresden.inf.st.sensorsharing.common.Constants.KEY_HEART_RATE;
import static de.tudresden.inf.st.sensorsharing.common.Constants.KEY_LINEAR_ACCELERATION;
import static de.tudresden.inf.st.sensorsharing.common.Constants.KEY_NAME;
import static de.tudresden.inf.st.sensorsharing.common.Constants.KEY_ROTATION_VECTOR;
import static de.tudresden.inf.st.sensorsharing.common.Constants.MQTT_CHARSET;
import static de.tudresden.inf.st.sensorsharing.common.Constants.SUB_KEY_AMBIENT_PRESSURE;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;


public class MainActivity extends WearableActivity implements SensorEventListener {
    private static final String TAG = "SensorSharing";

    private SensorManager mSensorManager;
    private Sensor mLight;
    private float mLightValue;
    private Sensor mLinearAcceleration;
    private float[] mLinearAccelerationValues;
    private Sensor mRotationVector;
    private float[] mRotationVectorValues;
    private boolean sendName = true;
    private Sensor mHeartRate;
    private float mHeartRateVaules;
    private Sensor mPressure;
    private float mPressureValues;
    private Sensor mCounter;
    private float mCounterValues;
    private long mShakeTime = 0;
    private static final int SHAKE_WAIT_TIME_MS = 250;
    private static final float SHAKE_THRESHOLD = 3.25f;
    private static int num = 0;

    private float mColor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // setup sensor manager and light sensor
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (mSensorManager == null) {
            TextView textView = findViewById(R.id.brightness_value);
            textView.setText(R.string.error_sensormanager_null);
            return;
        }
        // log available sensors
        List<Sensor> allSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        StringBuilder sb = new StringBuilder();

        for (Sensor sensor : allSensors) {
            sb.append(sensor.getName()).append(":").append(sensor.getStringType()).append(";");
        }
        Log.i(TAG, "Avail: able sensors: " + sb.toString());

        // initialize sensors
        mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        mLinearAcceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //mLinearAcceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mHeartRate = mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        mPressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        mCounter = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        if (mCounter ==null){System.out.println("not found counter");}
        if (mPressure ==null){System.out.println("not found pressure");}

        if (mLinearAcceleration == null) {
            mLinearAcceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            if (mRotationVector == null) {
                Log.e(TAG, "Couldn't find acceleration sensor!");
                TextView textView = findViewById(R.id.acceleration_value);
                textView.setText("ERROR");
            } else {
                Log.w(TAG, "Using alternative accelerometer sensor!");
            }
        }
        if(mHeartRate == null){
            System.out.println("not found Heart Rate Sensor");
        }
        mRotationVector = mSensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        if (mRotationVector == null) {
            mRotationVector = mSensorManager.getDefaultSensor(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR);
            if (mRotationVector == null) {
                TextView textView = findViewById(R.id.rotation_value);
                textView.setText("ERROR");
                Log.e(TAG, "Couldn't find rotation sensor!");
            } else {
                Log.w(TAG, "Using alternative rotation sensor!");
            }
        }
        // initialize sensor values
        mLinearAccelerationValues = new float[3];
        mRotationVectorValues = new float[3];
        // enables Always-on
        setAmbientEnabled();
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        TextView textView;
        switch(sensorEvent.sensor.getType()) {
            case Sensor.TYPE_PRESSURE:
                mPressureValues =sensorEvent.values[0];
                break;
            case Sensor.TYPE_STEP_COUNTER:
                mCounterValues = sensorEvent.values[0];
                break;
            case Sensor.TYPE_HEART_RATE:
                mHeartRateVaules = sensorEvent.values[0];
                break;
            case Sensor.TYPE_LIGHT:
                mLightValue = sensorEvent.values[0];
                textView = findViewById(R.id.brightness_value);
                textView.setText(String.valueOf(mLightValue));
                // a bit hacky: only send updates when brightness sensor changes
                sendCurrentValueToAllConnectedNodes();
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
            case Sensor.TYPE_ACCELEROMETER:
                mLinearAccelerationValues = sensorEvent.values;
                textView = findViewById(R.id.acceleration_value);
                textView.setText(Utils.formatArray3(mLinearAccelerationValues));
                detectShake(sensorEvent);
                if (num >3){
                    num = 0;
                }
                break;
            case Sensor.TYPE_GAME_ROTATION_VECTOR:
            case Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR:
                mRotationVectorValues = sensorEvent.values;
                textView = findViewById(R.id.rotation_value);
                textView.setText(Utils.formatArray3(mRotationVectorValues));
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            case KeyEvent.KEYCODE_NAVIGATE_NEXT:
                System.out.println("key down");
            case KeyEvent.KEYCODE_NAVIGATE_PREVIOUS:
                System.out.println("key up ");
        }
        return super.onKeyDown(keyCode, event);
    }

    private void detectShake(SensorEvent event) {
        //BoxInsetLayout layout = (BoxInsetLayout) findViewById(R.id.wear_layout);
        long now = System.currentTimeMillis();
        if((now - mShakeTime) > SHAKE_WAIT_TIME_MS) {
            mShakeTime = now;
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            if((Math.abs(x)>13||Math.abs(y)>13||Math.abs(z)>13)){
                num+=1;
                System.out.println("shake shake");
                View mContainerview = (BoxInsetLayout) findViewById(R.id.container);
                if (num == 1){
                    mContainerview.setBackgroundColor(Color.RED);
                    mColor = 1;
                    sendCurrentValueToAllConnectedNodes();
                }else if (num == 2){
                    mContainerview.setBackgroundColor(Color.BLUE);
                    mColor =2;
                    sendCurrentValueToAllConnectedNodes();
                }else if (num == 3){
                    mContainerview.setBackgroundColor(Color.GREEN);
                    mColor = 3;
                    sendCurrentValueToAllConnectedNodes();
                }else if (num == 4){
                    mColor = 4;
                    mContainerview.setBackgroundColor(Color.BLACK);
                    sendCurrentValueToAllConnectedNodes();
                }
            }
        }
    }


    private void sendCurrentValueToAllConnectedNodes() {
        Task<List<Node>> nodesTask = Wearable.getNodeClient(this).getConnectedNodes();
        final MessageClient messageClient = Wearable.getMessageClient(this);
        nodesTask.addOnSuccessListener(new OnSuccessListener<List<Node>>() {
            @Override
            public void onSuccess(List<Node> nodes) {
                final byte[] brightnessData = ByteBuffer.allocate(4).putFloat(mLightValue).array();
                final byte[] accelerationData = Utils.floatArray2ByteArray(mLinearAccelerationValues);
                final byte[] rotationData = Utils.floatArray2ByteArray(mRotationVectorValues);
                final byte[] heartRateData = ByteBuffer.allocate(4).putFloat(mHeartRateVaules).array();
                final byte[] pressure = ByteBuffer.allocate(4).putFloat(mPressureValues).array();
                final byte[] counter = ByteBuffer.allocate(4).putFloat(mCounterValues).array();
                final byte[] color = ByteBuffer.allocate(4).putFloat(mColor).array();


                for (Node node : nodes) {
                    if (sendName) {
                        final byte[] nameData = Build.MODEL.getBytes(MQTT_CHARSET);
                        messageClient.sendMessage(node.getId(), KEY_NAME, nameData);
                        //sendName = false;
                    }
                    messageClient.sendMessage(node.getId(), KEY_BRIGHTNESS, brightnessData);
                    messageClient.sendMessage(node.getId(), KEY_LINEAR_ACCELERATION, accelerationData);
                    messageClient.sendMessage(node.getId(), KEY_ROTATION_VECTOR, rotationData);
                    messageClient.sendMessage(node.getId(), KEY_HEART_RATE, heartRateData);
                    messageClient.sendMessage(node.getId(), KEY_COUNTER, counter);
                    messageClient.sendMessage(node.getId(), KEY_AMBIENT_PRESSURE, pressure);
                    messageClient.sendMessage(node.getId(), KEY_COLOR, color);
                }
            }
        });
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // ignore accuracy changes
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mLight, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mLinearAcceleration, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mRotationVector, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mHeartRate,SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mPressure, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mCounter, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    public void connectMqtt(View view) {
    }
}
