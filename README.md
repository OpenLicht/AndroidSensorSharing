# Android Sensor Sharing

## Data format

The data format used for publishing messages over MQTT is described.

Each group of data (orientation, brightness, light state) can be send individually or together by
ticking the corresponding checkbox or touching the toggle button inside the app.

JSON format is used to package the data.

Orientation and brightness are send continuously whereas the light state is only send by pressing the
toggle button.

### Brightness

- MQTT topic: ``sensors/phone/brightness``
- payload: ``{"brightness":67.0}``

### Orientation AHRS

- Orientation values are in the AHRS (yaw-pitch-roll angles)
- [Madwick](http://x-io.co.uk/open-source-imu-and-ahrs-algorithms/) algorithm is used

- MQTT topic: ``sensors/phone/rotationahrs``
- payload: ``{"roll":-168.21172,"pitch":22.937801,"heading":28.27002}``

### Orientation Quaternions

- Orientation values are in Quaternions

- MQTT topic: ``sensors/phone/rotation``
- payload: ``{"z":0.004691912,"x":0.0036029308,"y":-0.015808621}``

### Light State

- this is a one on/off thing. Message is only send when the toggle button is touched

- MQTT topic: ``sensors/phone/onoff``
- playload: ``{"on":true}``

## Use Cases

- IoSense (Smart Lighting Demo with the IoSense STK)
- OpenLicht