package de.tudresden.inf.st.sensorsharing.common;

import android.os.Build;

import org.apache.commons.lang.RandomStringUtils;

public class MqttUtils {

    public static String createClientId() {
        return String.format("%s-%s", Build.DEVICE, RandomStringUtils.randomAlphabetic(6));
    }
}
