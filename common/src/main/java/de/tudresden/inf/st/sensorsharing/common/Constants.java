package de.tudresden.inf.st.sensorsharing.common;

import java.nio.charset.Charset;

public class Constants {

    public static final String BASE_KEY = "/wearable/sensors/";
    public static final int BASE_KEY_LENGTH = BASE_KEY.length();

    public static final String SUB_KEY_BRIGHTNESS = "brightness";
    public static final String KEY_BRIGHTNESS = BASE_KEY + SUB_KEY_BRIGHTNESS;

    public static final String SUB_KEY_LINEAR_ACCELERATION = "acceleration";
    public static final String KEY_LINEAR_ACCELERATION = BASE_KEY + SUB_KEY_LINEAR_ACCELERATION;

    public static final String SUB_KEY_ROTATION_VECTOR = "rotation";
    public static final String KEY_ROTATION_VECTOR = BASE_KEY + SUB_KEY_ROTATION_VECTOR;

    public static final String SUB_KEY_HEART_RATE = "hear_rate";
    public static final String KEY_HEART_RATE = BASE_KEY + SUB_KEY_HEART_RATE;

    public static final String SUB_KEY_AMBIENT_PRESSURE = "hear_pressure";
    public static final String KEY_AMBIENT_PRESSURE = BASE_KEY + SUB_KEY_AMBIENT_PRESSURE;

    public static final String SUB_KEY_COUNTER = "hear_counter";
    public static final String KEY_COUNTER = BASE_KEY + SUB_KEY_COUNTER;

    public static final String SUB_KEY_COLOR = "color";
    public static final String KEY_COLOR = BASE_KEY + SUB_KEY_COLOR;

    public static final String SUB_KEY_NAME = "name";
    public static final String KEY_NAME = BASE_KEY + SUB_KEY_NAME;

    public static final Charset MQTT_CHARSET = Charset.forName("UTF-8");

}
