package de.tudresden.inf.st.sensorsharing.common;

import java.nio.ByteBuffer;
import java.util.Locale;

public class Utils {
    public static String formatArray3(float[] a) {
        return String.format(Locale.getDefault(), "%.2f:%.2f:%.2f", a[0], a[1], a[2]);
    }

    public static byte[] floatArray2ByteArray(float[] input) {
        ByteBuffer buffer = ByteBuffer.allocate(4 * input.length);
        for (float value : input) {
            buffer.putFloat(value);
        }
        return buffer.array();
    }
}
