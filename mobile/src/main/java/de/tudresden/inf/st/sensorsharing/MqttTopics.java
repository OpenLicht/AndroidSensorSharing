package de.tudresden.inf.st.sensorsharing;

public class MqttTopics {
    private static final String MQTT_TOPIC_BASE = "sensors/";
    public final String mqtt_topic_brightness;
    public final String mqtt_topic_acceleration;
    public final String mqtt_topic_rotation;
    public final String mqtt_topic_rotation_ahrs;
    public final String mqtt_topic_wear_hear_rate;
    public final String mqtt_topic_air_pressure;
    public final String mqtt_topic_step_counter;
    public final String mqtt_topic_onoff;

    public MqttTopics(String subTopic) {
        mqtt_topic_brightness = MQTT_TOPIC_BASE + subTopic + "/brightness";
        mqtt_topic_acceleration = MQTT_TOPIC_BASE + subTopic + "/acceleration";
        mqtt_topic_rotation = MQTT_TOPIC_BASE + subTopic + "/rotation";
        mqtt_topic_rotation_ahrs = MQTT_TOPIC_BASE + subTopic + "/rotationahrs";
        mqtt_topic_onoff = MQTT_TOPIC_BASE + subTopic + "/onoff";
        // wear topic
        mqtt_topic_wear_hear_rate = MQTT_TOPIC_BASE + subTopic +"/heart_rate";
        mqtt_topic_air_pressure = MQTT_TOPIC_BASE + subTopic + "/air_pressure";
        mqtt_topic_step_counter =MQTT_TOPIC_BASE +subTopic+"/step_counter";
    }
}
