package de.tudresden.inf.st.sensorsharing.settings;

import android.content.Context;
import android.content.SharedPreferences;

public class AppSettings {
    public final static String SETTINGS_MQTT_SERVER = "mqtt-server";

    Context context;
    SharedPreferences.Editor editor;
    SharedPreferences appsettings;
    public AppSettings(Context context) {
        this.context = context;
        this.appsettings = context.getSharedPreferences("appsettings", 0);
    }

    public void store(String key, String value) {
        editor = appsettings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String get(String key, String defaultValue) {
        return appsettings.getString(key, defaultValue);
    }
}
