package de.tudresden.inf.st.sensorsharing;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.wearable.MessageClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import de.tudresden.inf.st.sensorsharing.common.MadgwickAHRS;
import de.tudresden.inf.st.sensorsharing.common.MqttUtils;
import de.tudresden.inf.st.sensorsharing.common.Utils;
import de.tudresden.inf.st.sensorsharing.settings.AppSettings;


import static de.tudresden.inf.st.sensorsharing.common.Constants.BASE_KEY;
import static de.tudresden.inf.st.sensorsharing.common.Constants.BASE_KEY_LENGTH;
import static de.tudresden.inf.st.sensorsharing.common.Constants.MQTT_CHARSET;
import static de.tudresden.inf.st.sensorsharing.common.Constants.SUB_KEY_AMBIENT_PRESSURE;
import static de.tudresden.inf.st.sensorsharing.common.Constants.SUB_KEY_BRIGHTNESS;
import static de.tudresden.inf.st.sensorsharing.common.Constants.SUB_KEY_COLOR;
import static de.tudresden.inf.st.sensorsharing.common.Constants.SUB_KEY_COUNTER;
import static de.tudresden.inf.st.sensorsharing.common.Constants.SUB_KEY_HEART_RATE;
import static de.tudresden.inf.st.sensorsharing.common.Constants.SUB_KEY_LINEAR_ACCELERATION;
import static de.tudresden.inf.st.sensorsharing.common.Constants.SUB_KEY_NAME;
import static de.tudresden.inf.st.sensorsharing.common.Constants.SUB_KEY_ROTATION_VECTOR;
import static de.tudresden.inf.st.sensorsharing.settings.AppSettings.SETTINGS_MQTT_SERVER;
import static java.lang.Math.atan2;
import static java.lang.Math.floor;
//bbq

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.BottomNavigationView;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
public class MainActivity extends AppCompatActivity implements
        SensorEventListener,
        MessageClient.OnMessageReceivedListener, First.OnFragmentInteractionListener, Second.OnFragmentInteractionListener,Third.OnFragmentInteractionListener{
    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    //in/E_Motion_Detector/state CLOSED

    private static final String TAG = "SensorSharing";
//    private static final String MQTT_TOPIC_SAMSUNG = "sensors/samsung/brightness";
    private static String return_value_seekbar = "";
    // sensor management
    private SensorManager mSensorManager;
    private Sensor mLight;
    private Sensor mRotationVector;
    private Sensor gyroSensor;
    private Sensor accelSensor;
    private Sensor magSensor;

    // mqtt management
    private static final int MQTT_DEFAULT_QOS = 0;
    private MqttAndroidClient mqttAndroidClient;
    private MqttTopics wearTopics;
    private MqttTopics phoneTopics;
    private int sensorChangedCounter = 0;
    private final ConcurrentHashMap<String, Boolean> sendMqttUpdates = new ConcurrentHashMap<>();

    // wearable management
    /**
     * Delay in milliseconds after which the wearable is assumed to be offline
     */
    public static final int DELAY_WEARABLE_ASSUMED_OFFLINE = 30000;
    private Date lastMessageFromWearable = new Date();
    private DateFormat dateTimeFormat = DateFormat.getDateTimeInstance(
            DateFormat.SHORT, android.icu.text.DateFormat.SHORT);

    // sensor values of phone
    private float[] mRotationVectorValues;
    private float[] gyroVectorValues;
    private float[] accelVectorValues;
    private float[] magVectorValues;
    private float mLightValue;
    private String clientId;
    private AppSettings settings;
    MadgwickAHRS filter;
    Button mqtt_button;
    ToggleButton color_button;
    private boolean isChecked_colorControl;

    private EditText valueServerURI;
    // fragments variable
    final Fragment fragment1 = new First();
    final Fragment fragment2 = new Second();
    final Fragment fragment3 = new Third();
    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = fragment1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.setProperty("log.tag." + TAG, "INFO");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        settings = new AppSettings(this);

        initMqttClientId();
        updateMqttTopics("unknown");
        filter = new MadgwickAHRS(50, 1f);

        // Bottom Navigation
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fm.beginTransaction().add(R.id.main_container, fragment3, "3").hide(fragment3).commit();
        fm.beginTransaction().add(R.id.main_container, fragment2, "2").hide(fragment2).commit();
        fm.beginTransaction().add(R.id.main_container,fragment1, "1").commit();


        // setup sensor manager and ic_light sensor
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (mSensorManager == null) {
            Toast.makeText(this, "Could not fetch sensor manager", Toast.LENGTH_SHORT).show();
            return;
        }
        mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        gyroSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mRotationVector = mSensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        magSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
            accelSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        }
        if (mRotationVector == null) {
            mRotationVector = mSensorManager.getDefaultSensor(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR);
            if (mRotationVector == null) {
                TextView textView = findViewById(R.id.value_own_rotation);
                textView.setText("ERROR");
                Log.e(TAG, "Couldn't find rotation sensor!");
            } else {
                Log.w(TAG, "Using alternative rotation sensor!");
            }
        }
        // initialize sensor values
        mLightValue = -1;
        mRotationVectorValues = new float[3];
        accelVectorValues = new float[3];
        gyroVectorValues = new float[3];
        magVectorValues = new float[3];

        // repeating connectivity check
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long now = System.currentTimeMillis();
                if (now - lastMessageFromWearable.getTime() > DELAY_WEARABLE_ASSUMED_OFFLINE) {
                    updateWearStatus(false);
                }
                // repeat after delay
                handler.postDelayed(this, DELAY_WEARABLE_ASSUMED_OFFLINE);
            }
        };
        handler.post(runnable);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initViews();
        updateWearStatus(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return super.onCreateOptionsMenu(menu);
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fm.beginTransaction().hide(active).show(fragment1).commit();
                    active = fragment1;
                    return true;

                case R.id.navigation_dashboard:
                    fm.beginTransaction().hide(active).show(fragment2).commit();
                    active = fragment2;
                    return true;

                case R.id.navigation_notifications:
                    fm.beginTransaction().hide(active).show(fragment3).commit();
                    active = fragment3;
                    return true;
            }
            return false;
        }
    };

    private void initViews() {
        setName();
        valueServerURI = findViewById(R.id.value_server_uri);

        String mqttServer = settings.get(SETTINGS_MQTT_SERVER, getResources().getString(R.string.default_mqtt_server_uri));
        valueServerURI.setText(mqttServer);
        //inital for mqtt button
        mqtt_button = findViewById(R.id.button);
        mqtt_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connectMqtt(view);
            }
        });
        color_button = findViewById(R.id.color_control_button);
        color_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    isChecked_colorControl =b;
            }
        });
        setupLightsController();
        setupSendUpdatesCheckbox();
        setupLightsController();
    }

    private void setupLightsController(){

        final String openhab_topic = "oh2/in/iris1_item";
        //final String openhab_topic1 = "oh2/out/iris1_item";
        final String home_e_lights_topic = "out/E_Lights_Analog/state";
        final String home_d_lights_1_topic = "out/D_Lights_1_Analog/state";
        final String home_d_lights_2_topic = "out/D_Lights_2_Analog/state";
        final String home_g_lights_topic = "out/G_Lights_Analog/state";

        final CheckBox hue_1 = findViewById(R.id.checkbox_openhab_light_1);
        final CheckBox e_lights = findViewById(R.id.checkbox_e_lights);
        final CheckBox d_lights1 = findViewById(R.id.checkbox_d_lights_1);
        final CheckBox d_lights2= findViewById(R.id.checkbox_d_lights_2);
        final CheckBox g_lights = findViewById(R.id.checkbox_g_lights);

        CompoundButton.OnCheckedChangeListener onCheckedChangeListener= new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                switch (compoundButton.getId()){
                    case R.id.checkbox_openhab_light_1:
                        //sendMqttUpdates.put(openhab_topic1, isChecked);
                        sendMqttUpdates.put(openhab_topic, isChecked);
                        SeekBar openhab_seekbar = findViewById(R.id.openhab_seekBar_1);
                        openhab_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                return_value_seekbar = String.valueOf(i);
                                sendUpdate(openhab_topic,"7,"+"100,"+String.valueOf(i));
                                //sendUpdate(openhab_topic1,"7,"+"100,"+String.valueOf(i));
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {
                                System.out.println("start tracking");
                            }

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {
                                System.out.println("stop tracking");
                            }
                        });
                        break;
                    case R.id.checkbox_e_lights:
                        sendMqttUpdates.put(home_e_lights_topic, isChecked);
                        SeekBar e_lights_seekbar = findViewById(R.id.e_lights_seekbar);
                        e_lights_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                sendUpdate(home_e_lights_topic,String.valueOf(i));
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {

                            }

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {

                            }
                        });
                        break;
                    case R.id.checkbox_d_lights_1:
                        SeekBar d_lights1_seekbar = findViewById(R.id.d_lights1_seekbar);
                        d_lights1_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                sendUpdate(home_d_lights_1_topic, String.valueOf(i));
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {

                            }

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {

                            }
                        });
                        sendMqttUpdates.put(home_d_lights_1_topic, isChecked);
                        break;
                    case R.id.checkbox_d_lights_2:
                        SeekBar d_lights2_seebar = findViewById(R.id.d_lights2_seekbar);
                        d_lights2_seebar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                sendUpdate(home_d_lights_2_topic, String.valueOf(i));
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {

                            }

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {

                            }
                        });
                        sendMqttUpdates.put(home_d_lights_2_topic, isChecked);
                        break;
                    case R.id.checkbox_g_lights:
                        SeekBar g_lights_seekbar = findViewById(R.id.g_lights_seekbar);
                        g_lights_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                sendUpdate(home_g_lights_topic, String.valueOf(i));
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {

                            }

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {

                            }
                        });
                        sendMqttUpdates.put(home_g_lights_topic, isChecked);
                        break;

                }
            }
        };
        hue_1.setOnCheckedChangeListener(onCheckedChangeListener);
        e_lights.setOnCheckedChangeListener(onCheckedChangeListener);
        d_lights1.setOnCheckedChangeListener(onCheckedChangeListener);
        d_lights2.setOnCheckedChangeListener(onCheckedChangeListener);
        g_lights.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    private void updateMqttTopics(String wearableName) {
        // moto: "Moto 360"
        // polar: ??
        String subTopic;
        switch (wearableName) {
            case "Moto 360":
                subTopic = "moto360";
                break;
            default:
                // should be a specific name for the Polar, once the app runs again
                subTopic = "polar";
        }
        wearTopics = new MqttTopics(subTopic);
        // use for phone model for now
        phoneTopics = new MqttTopics("samsung");
    }

    private void setupSendUpdatesCheckbox() {
        final CheckBox sendUpdates = findViewById(R.id.checkBox_send_brightness);
        final CheckBox sendUpdatesRotation = findViewById(R.id.checkBox_send_rotation_ahrs);
        final CheckBox sendUpdatesRotationQuat = findViewById(R.id.checkBox_send_rotation_quaternion);
        final CheckBox sendUpdatesAcceleration = findViewById(R.id.checkBox_send_acceleration);

        final CheckBox sendUpdatesWearBrightness = findViewById(R.id.checkBox_send_wear_brightness);
        final CheckBox sendUpdatesWearAcc= findViewById(R.id.checkBox_send_wear_acceleration);
        final CheckBox sendUpdatesWearRotation= findViewById(R.id.checkBox_send_wear_rotation);
        final CheckBox sendUpdatesWearAirPressure = findViewById(R.id.checkBox_send_wear_air_pressure);
        final CheckBox sendUpdatesWearHeartRate = findViewById(R.id.checkBox_send_wear_heart_rate);
        final CheckBox sendUpdatesStepCounter = findViewById(R.id.checkBox_send_wear_counter);

        sendMqttUpdates.put(phoneTopics.mqtt_topic_brightness, sendUpdates.isChecked());
        sendMqttUpdates.put(phoneTopics.mqtt_topic_rotation_ahrs, sendUpdatesRotation.isChecked());
        sendMqttUpdates.put(phoneTopics.mqtt_topic_rotation, sendUpdatesRotationQuat.isChecked());
        sendMqttUpdates.put(phoneTopics.mqtt_topic_acceleration, sendUpdatesAcceleration.isChecked());

        sendMqttUpdates.put(wearTopics.mqtt_topic_brightness, sendUpdatesWearBrightness.isChecked());
        sendMqttUpdates.put(wearTopics.mqtt_topic_acceleration, sendUpdatesWearAcc.isChecked());
        sendMqttUpdates.put(wearTopics.mqtt_topic_rotation, sendUpdatesWearRotation.isChecked());
        sendMqttUpdates.put(wearTopics.mqtt_topic_air_pressure, sendUpdatesWearAirPressure.isChecked());
        sendMqttUpdates.put(wearTopics.mqtt_topic_step_counter, sendUpdatesStepCounter.isChecked());

        CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                switch (compoundButton.getId()) {
                    case R.id.checkBox_send_brightness:
                        sendMqttUpdates.put(phoneTopics.mqtt_topic_brightness, isChecked);
                        break;
                    case R.id.checkBox_send_rotation_ahrs:
                        sendMqttUpdates.put(phoneTopics.mqtt_topic_rotation_ahrs, sendUpdatesRotation.isChecked());
                        break;
                    case R.id.checkBox_send_rotation_quaternion:
                        sendMqttUpdates.put(phoneTopics.mqtt_topic_rotation, sendUpdatesRotationQuat.isChecked());
                        break;
                        //test wear
                    case R.id.checkBox_send_acceleration:
                        sendMqttUpdates.put(phoneTopics.mqtt_topic_acceleration, sendUpdatesAcceleration.isChecked());
                        break;
                    case R.id.checkBox_send_wear_brightness:
                        sendMqttUpdates.put(wearTopics.mqtt_topic_brightness, sendUpdatesWearBrightness.isChecked());
                        break;
                    case R.id.checkBox_send_wear_acceleration:
                        sendMqttUpdates.put(wearTopics.mqtt_topic_acceleration, sendUpdatesWearAcc.isChecked());
                        break;
                    case R.id.checkBox_send_wear_rotation:
                        sendMqttUpdates.put(wearTopics.mqtt_topic_rotation, sendUpdatesWearRotation.isChecked());
                        break;
                    case R.id.checkBox_send_wear_air_pressure:
                        sendMqttUpdates.put(wearTopics.mqtt_topic_air_pressure, sendUpdatesWearAirPressure.isChecked());
                        break;
                    case R.id.checkBox_send_wear_heart_rate:
                        sendMqttUpdates.put(wearTopics.mqtt_topic_wear_hear_rate, sendUpdatesWearHeartRate.isChecked());
                        break;
                    case R.id.checkBox_send_wear_counter:
                        sendMqttUpdates.put(wearTopics.mqtt_topic_step_counter, sendUpdatesStepCounter.isChecked());
                        break;
                }
            }
        };
        sendUpdates.setOnCheckedChangeListener(onCheckedChangeListener);
        sendUpdatesRotation.setOnCheckedChangeListener(onCheckedChangeListener);
        sendUpdatesRotationQuat.setOnCheckedChangeListener(onCheckedChangeListener);
        sendUpdatesAcceleration.setOnCheckedChangeListener(onCheckedChangeListener);
        sendUpdatesWearBrightness.setOnCheckedChangeListener(onCheckedChangeListener);
        sendUpdatesWearAcc.setOnCheckedChangeListener(onCheckedChangeListener);
        sendUpdatesWearRotation.setOnCheckedChangeListener(onCheckedChangeListener);
        sendUpdatesStepCounter.setOnCheckedChangeListener(onCheckedChangeListener);
        sendUpdatesWearHeartRate.setOnCheckedChangeListener(onCheckedChangeListener);
        sendUpdatesWearAirPressure.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    private void setName() {
        TextView ownName = findViewById(R.id.value_own_name);
        TextView wearName = findViewById(R.id.value_wear_name);
        System.out.println("test model"+Build.MODEL);
        ownName.setText(Build.MODEL);

    }

    private void closeKeyboard() {
        if (this.getCurrentFocus() != null) {
            InputMethodManager inputManager =
                    (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            Objects.requireNonNull(inputManager).hideSoftInputFromWindow(
                    this.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void connectMqtt(View view) {
        closeKeyboard();
        settings.store(SETTINGS_MQTT_SERVER, valueServerURI.getText().toString());
        // disconnect if there is a connected client
        if (mqttAndroidClient != null && mqttAndroidClient.isConnected()) {
            try {
                mqttAndroidClient.disconnect();
            } catch (MqttException e) {
                Toast.makeText(MainActivity.this,
                        "Could not disconnect from running broker. Continue.",
                        Toast.LENGTH_SHORT).show();
            }
        }

        // setup new mqtt client
        final String serverURI = valueServerURI.getText().toString();
        Toast.makeText(MainActivity.this, "Connecting to " + serverURI,
                Toast.LENGTH_SHORT).show();
        System.out.println(serverURI);
        mqttAndroidClient = new MqttAndroidClient(this, serverURI, clientId);

        mqttAndroidClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {
                Toast.makeText(MainActivity.this, "Connection to MQTT broker lost",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) {

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });
        final MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(false);
        options.setAutomaticReconnect(true);
        options.setConnectionTimeout(5);
        try {
            IMqttToken token = mqttAndroidClient.connect(options,
                    null, new IMqttActionListener() {
                        @Override
                        public void onSuccess(IMqttToken asyncActionToken) {
                            Log.i(TAG, "Successfully connected to " + serverURI);

                            try{mqttAndroidClient.subscribe("in/#",MQTT_DEFAULT_QOS);}catch (MqttException e){
                               System.out.println("subscribe not successful");
                            }
                           mqttAndroidClient.setCallback(new MqttCallback() {
                                @Override
                                public void connectionLost(Throwable cause) {

                                }

                                @Override
                                public void messageArrived(String topic, MqttMessage message) throws Exception {
                                    String status_message= new String(message.getPayload());
                                    TextView textView = findViewById(R.id.room_name);
                                    TextView textView1 = findViewById(R.id.current_light_name);
                                    System.out.println(" Topic:\t" + topic +
                                            " Message:\t" + status_message +" QoS:\t" + message.getQos() );
                                    if (topic.equals("in/E_Motion_Detector/state") && status_message.equals("OPEN")){
                                        textView.setText("Entrance Room");
                                        textView1.setText("E Lights");
                                    } else if (topic.equals("in/G_Motion_Detector/state") && status_message.equals("OPEN")){

                                        textView.setText("Bedroom Corridor");
                                        textView1.setText("G Lights");
                                    } else if(topic.equals("in/D_Motion_Detector/state") && status_message.equals("OPEN")){
                                        textView.setText("Kitchen");
                                        textView1.setText("D Lights 1 and Lights 2");
                                    }
                                    SeekBar current_seekbar = findViewById(R.id.seekBar1);
                                    current_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                        @Override
                                        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                            TextView textView = findViewById(R.id.room_name);
                                            String room = textView.getText().toString();
                                            if(room.equals("Entrance Room")){
                                                System.out.println("i want control entrance room" + String.valueOf(i));
                                                sendMqttUpdates.put("out/E_Lights_Analog/state",true);
                                                sendUpdate("out/E_Lights_Analog/state",String.valueOf(i));
                                            }else if(room.equals("Bedroom Corridor")){
                                                sendMqttUpdates.put("out/G_Lights_Analog/state",true);
                                                sendUpdate("out/G_Lights_Analog/state",String.valueOf(i));
                                            }else if(room.equals("Kitchen")){
                                                sendMqttUpdates.put("out/D_Lights_1_Analog/state",true);
                                                sendMqttUpdates.put("out/D_Lights_2_Analog/state",true);
                                                sendUpdate("out/D_Lights_1_Analog/state",String.valueOf(i));
                                                sendUpdate("out/D_Lights_2_Analog/state",String.valueOf(i));
                                            }
                                        }

                                        @Override
                                        public void onStartTrackingTouch(SeekBar seekBar) {

                                        }

                                        @Override
                                        public void onStopTrackingTouch(SeekBar seekBar) {

                                        }
                                    });
                                }


                                @Override
                                public void deliveryComplete(IMqttDeliveryToken token) {

                                }
                            });
                        }

                        @Override
                        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                            Log.e(TAG, "Connection to " + serverURI + " failed", exception);
                            Toast.makeText(MainActivity.this, "Connection to " + serverURI + " failed",
                                    Toast.LENGTH_SHORT).show();
                            options.setAutomaticReconnect(false);
                        }
                    });

        } catch (MqttException e) {
            Toast.makeText(this, "Connection to " + serverURI + " failed",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void updateWearStatus(boolean connected) {
        String text;
        if (connected) {
            text = "Connected (last contact: %s)";
        } else {
            text = "Disconnected since %s";
        }
        TextView wearStatus = findViewById(R.id.value_wear_status);
        System.out.println("Test debug"+String.format(text, dateTimeFormat.format(lastMessageFromWearable)));
        System.out.println("wear status object"+wearStatus);
        //TODO:
        try{
            wearStatus.setText(String.format(text, dateTimeFormat.format(lastMessageFromWearable)));
        }catch (NullPointerException e){System.out.println(e);}

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        TextView textView;
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_LIGHT:
                mLightValue = sensorEvent.values[0];
                textView = findViewById(R.id.value_own_brightness);
                textView.setText(String.valueOf(mLightValue));
                break;
            case Sensor.TYPE_GAME_ROTATION_VECTOR:
            case Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR:
                mRotationVectorValues = sensorEvent.values;
                textView = findViewById(R.id.value_own_rotation);
                textView.setText(Utils.formatArray3(mRotationVectorValues));
                break;
            case Sensor.TYPE_ACCELEROMETER:
                accelVectorValues = sensorEvent.values;
                textView = findViewById(R.id.value_own_acceleration);
                textView.setText(Utils.formatArray3(accelVectorValues));
            case Sensor.TYPE_GYROSCOPE:
                gyroVectorValues = sensorEvent.values;
                break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                magVectorValues = sensorEvent.values;
                break;
        }
        if (++sensorChangedCounter >= 15) {
            // only send every 15th change
            sensorChangedCounter = 0;
            sendUpdateOfSmartphone();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // ignore accuracy changes
    }
    @Override
    protected void onResume() {
        super.onResume();
        initMqttClientId();
        mSensorManager.registerListener(this, mLight, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, mRotationVector, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, accelSensor, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, gyroSensor, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, magSensor, SensorManager.SENSOR_DELAY_UI);
        Wearable.getMessageClient(this).addListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
        Wearable.getMessageClient(this).removeListener(this);
    }

    private void initMqttClientId() {
        if (clientId == null || clientId.isEmpty()) {
            clientId = MqttUtils.createClientId();
        }
    }

    @Override
    public void onMessageReceived(@NonNull final MessageEvent messageEvent) {
        final String openhab_topic = "oh2/in/iris1_item";
        final String openhab_topic1= "oh2/out/iris1_item";
        String path = messageEvent.getPath();
        if (path == null) return;
        if (path.startsWith(BASE_KEY)) {
            lastMessageFromWearable = new Date();
            final String updatedValue;
            String topic = null;
            final TextView textViewToUpdate;
            final byte[] bytesToSendViaMqtt = messageEvent.getData();

            switch (path.substring(BASE_KEY_LENGTH)) {
                case SUB_KEY_COLOR:
                    float mColor = ByteBuffer.wrap(messageEvent.getData()).getFloat();
                    String updateColor = Float.toString(mColor);
                    TextView color = findViewById(R.id.label_openhab_button);
                    if(isChecked_colorControl){
                        sendMqttUpdates.put(openhab_topic, isChecked_colorControl);
                        //sendMqttUpdates.put(openhab_topic1, isChecked_colorControl);
                        if(mColor == 1){
                            color.setTextColor(Color.RED);
                            sendUpdate(openhab_topic,"7,100,"+return_value_seekbar);
                            //sendUpdate(openhab_topic1,"7,100,"+return_value_seekbar);

                        }else if(mColor==2){
                            color.setTextColor(Color.BLUE);
                            sendUpdate(openhab_topic,"240,100,"+return_value_seekbar);
                            //sendUpdate(openhab_topic1,"240,100,"+return_value_seekbar);
                        }else if(mColor == 3){
                            color.setTextColor(Color.GREEN);
                            sendUpdate(openhab_topic,"130,100,"+return_value_seekbar);
                            //sendUpdate(openhab_topic1,"130,100,"+return_value_seekbar);
                        }else {color.setTextColor(Color.GRAY);
                            sendUpdate(openhab_topic,"7,100,"+return_value_seekbar);
                            //sendUpdate(openhab_topic1,"7,100,"+return_value_seekbar);
                        }
                    }

                case SUB_KEY_COUNTER:
                    float mCounter = ByteBuffer.wrap(messageEvent.getData()).getFloat();
                    updatedValue = Float.toString(mCounter);
                    textViewToUpdate = findViewById(R.id.value_wear_counter);
                    topic = wearTopics.mqtt_topic_step_counter;
                    break;
                case SUB_KEY_AMBIENT_PRESSURE:
                    float mAmbientPressure = ByteBuffer.wrap(messageEvent.getData()).getFloat();
                    updatedValue = Float.toString(mAmbientPressure);
                    textViewToUpdate = findViewById(R.id.value_wear_pressure);
                    topic = wearTopics.mqtt_topic_air_pressure;
                    break;
                case SUB_KEY_HEART_RATE:
                    float mHeatBeatValues = ByteBuffer.wrap(messageEvent.getData()).getFloat();
                    updatedValue=Float.toString(mHeatBeatValues);
                    textViewToUpdate = findViewById(R.id.value_wear_heart_rate);
                    topic = wearTopics.mqtt_topic_wear_hear_rate;
                    break;
                case SUB_KEY_BRIGHTNESS:
                    float mWearBrightness = ByteBuffer.wrap(messageEvent.getData()).getFloat();
                    updatedValue = Float.toString(mWearBrightness);
                    textViewToUpdate = findViewById(R.id.value_wear_brightness);
                    topic = wearTopics.mqtt_topic_brightness;
                    //updateMqttTopics(updatedValue);
                    break;
                case SUB_KEY_LINEAR_ACCELERATION:
//                    float[] accelerationData = new float[3];
//                    writeFloatArray(ByteBuffer.wrap(messageEvent.getData()), accelerationData);
//                    updatedValue = Arrays.toString(accelerationData)
                    updatedValue = formatByteFloatArray(ByteBuffer.wrap(messageEvent.getData()));
                    textViewToUpdate = findViewById(R.id.value_wear_acceleration);
                    topic = wearTopics.mqtt_topic_acceleration;
                    //updateMqttTopics(updatedValue);
                    break;
                case SUB_KEY_ROTATION_VECTOR:
//                    float[] rotationData = new float[3];
//                    writeFloatArray(ByteBuffer.wrap(messageEvent.getData()), rotationData);
//                    updatedValue = Arrays.toString(rotationData);
                    updatedValue = formatByteFloatArray(ByteBuffer.wrap(messageEvent.getData()));
                    textViewToUpdate = findViewById(R.id.value_wear_rotation);
                    topic = wearTopics.mqtt_topic_rotation;
                    //updateMqttTopics(updatedValue);
                    break;
                case SUB_KEY_NAME:
                    updatedValue = new String(messageEvent.getData(), MQTT_CHARSET);
                    textViewToUpdate = findViewById(R.id.value_wear_name);
                    updateMqttTopics(updatedValue);
                    break;
                default:
                    Log.w(TAG, "Unknown path: " + path);
                    return;
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateWearStatus(true);
                    textViewToUpdate.setText(updatedValue);

                }
            });
            if (topic != null) {
                //sendUpdateOfWearable(topic, bytesToSendViaMqtt);
                sendUpdateOfWearable(topic, updatedValue);

            }
        }
    }

    private String formatByteFloatArray(ByteBuffer buffer) {

        float[] arr = new float[3];
        arr[0]=buffer.asFloatBuffer().get(0);
        arr[1]=buffer.asFloatBuffer().get(1);
        arr[2]=buffer.asFloatBuffer().get(2);
        return formatFloatArray(arr);
    }

    private String formatFloatArray(float... values) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < values.length; i++) {
            if (i > 0) {
                sb.append(" : ");
            }
            sb.append(String.format(Locale.getDefault(), "%.2f", values[i]));
        }
        return sb.toString();
    }

    private void writeFloatArray(ByteBuffer input, float[] output) {
        for (int i = 0; i < output.length; i++) {
            output[i] = input.getFloat();
        }
    }

    private void sendUpdateOfWearable(String topic, String newValue) {
        sendUpdate(topic, newValue);
    }
    /**
     * Method to initiate the mqtt publish action of the required sensor data.
     * Whether the data is really published depends on the checkboxes.
     */
    private void sendUpdateOfSmartphone() {

        ObjectMapper mapper = new ObjectMapper();

        // Brightness data
        HashMap<String, Float> dataLight = new HashMap<>();
        dataLight.put("brightness", mLightValue);

        try {
            String s = mapper.writeValueAsString(dataLight);
            sendUpdate(phoneTopics.mqtt_topic_brightness, String.valueOf(mLightValue) );
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        // Acceleration data
        HashMap<String, Float> acceleration = new HashMap<>();
        acceleration.put("x", accelVectorValues[0]);
        acceleration.put("y", accelVectorValues[1]);
        acceleration.put("z", accelVectorValues[2]);
        try {
            String s = mapper.writeValueAsString(acceleration);
            sendUpdate(phoneTopics.mqtt_topic_acceleration, String.valueOf(acceleration));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        // AHRS data
        filter.Update(gyroVectorValues[0], gyroVectorValues[1], gyroVectorValues[2],
                accelVectorValues[0], accelVectorValues[1], accelVectorValues[2],
                magVectorValues[0], magVectorValues[1], magVectorValues[2]);
        filter.computeAngles();
        float yaw = filter.getYaw();
        float pitch = filter.getPitch();
        float roll = filter.getRoll();
        //System.out.println(String.format("%f, %f, %f", yaw, pitch, roll));
        HashMap<String, Float> data = new HashMap<>();
        data.put("heading", yaw);
        data.put("pitch", pitch);
        data.put("roll", roll);
        try {
            String s = mapper.writeValueAsString(data);
            sendUpdate(phoneTopics.mqtt_topic_rotation_ahrs, String.valueOf(data));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        // Quaternion data
        HashMap<String, Float> dataQuat = new HashMap<>();
        dataQuat.put("x", mRotationVectorValues[0]);
        dataQuat.put("y", mRotationVectorValues[1]);
        dataQuat.put("z", mRotationVectorValues[2]);
        //dataQuat.put("w", mRotationVectorValues[3]);
        try {
            String s = mapper.writeValueAsString(dataQuat);
            sendUpdate(phoneTopics.mqtt_topic_rotation, String.valueOf(dataQuat));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private void sendUpdate(String topic, String newValue) {
        sendUpdate(topic, newValue.getBytes(MQTT_CHARSET));
    }

    private void sendUpdate(String topic, byte[] bytes) {
        if (sendMqttUpdates.get(topic) == null || !sendMqttUpdates.get(topic)) {

            return;
        }
        if (mqttAndroidClient == null) {
            Log.d(TAG, "mqtt client is null");
            return;
        }
        if (!mqttAndroidClient.isConnected()) {
//            Log.v(TAG, "mqtt client not connected");
            return;
        }
        // send to MQTT
        try {
            mqttAndroidClient.publish(topic, bytes, MQTT_DEFAULT_QOS, false);
        } catch (MqttException e) {
            Log.d(TAG, "mqtt message publish failed", e);
        }
    }
}
